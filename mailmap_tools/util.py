#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Miscellaneous functions without a better location."""


def unwrap_email(email: str):
    """Remove leading and trailing space and <> brackets."""
    stripped = email.strip()
    if stripped.startswith("<") and stripped.endswith(">"):
        return stripped[1:-1]
    return stripped


def format_person(name: str, email: str) -> str:
    """Format a name, email pair as a string."""
    if email.startswith("<") and email.endswith(">"):
        return f"{name} {email}"
    return f"{name} <{email}>"
