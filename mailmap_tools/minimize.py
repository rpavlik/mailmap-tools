#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Minimize a mailmap file by omitting redundant lines."""
from argparse import ArgumentParser
import sys
from .types import MailmapLine
from .output import MailmapStateForMinimizing


def main(filename):
    """Minimize a mailmap file."""
    lines: list[MailmapLine] = []

    print("Opening mailmap file", file=sys.stderr)
    with open(filename, "r", encoding="utf-8") as fp:
        for line in fp:
            parsed = MailmapLine(line)
            lines.append(parsed)

    print(
        "All lines parsed, now transforming into operations and accumulating...",
        file=sys.stderr,
    )

    state = MailmapStateForMinimizing()
    for line in lines:
        state.add(line)

    print("Generating output...", file=sys.stderr)

    output = state.to_mailmap_lines()
    print(output)


def script():
    """Run checker from command line."""
    parser = ArgumentParser()
    parser.add_argument("filename", help="Mailmap file name", nargs=1)
    args = parser.parse_args()
    main(args.filename[0])


if __name__ == "__main__":
    script()
