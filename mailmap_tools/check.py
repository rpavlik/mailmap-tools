#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Check a mailmap file for inconsistencies requiring user intervention."""
from argparse import ArgumentParser
import sys
from collections import defaultdict
from typing import Iterable, Optional, cast

from .types import MailmapLine, OperationType
from .util import format_person


def main(filename):
    """Check a mailmap file for consistency."""
    lines: list[MailmapLine] = []

    print("Opening mailmap file")
    with open(filename, "r", encoding="utf-8") as fp:
        for line in fp:
            parsed = MailmapLine(line)
            lines.append(parsed)

    print("All lines parsed, now transforming into operations in the checker...")

    checker = MailmapChecker()
    for num, line in enumerate(lines, 1):
        checker.add(line, line_number=num)

    print("Now checking accumulated operations for issues...")
    warnings = checker.check_for_warnings()

    if warnings > 0:
        sys.exit(1)


class MailmapChecker:
    """Checks for consistency errors in a mailmap file."""

    def __init__(self):
        """Construct object."""
        # casefolded email to canonical email
        self.canonicalize_emails: dict[str, set[str]] = defaultdict(set)

        # casefolded email to canonical name
        self.names: dict[str, set[str]] = defaultdict(set)

        # casefolded (name, email) to canonical (name, email)
        self.full_matches: dict[tuple[str, str], set[tuple[str, str]]] = defaultdict(
            set
        )

        self.line_numbers_for_email: dict[str, set[int]] = defaultdict(set)
        self.line_numbers_for_pair: dict[tuple[str, str], set[int]] = defaultdict(set)

    def _record_line_number_for_email(
        self, casefolded_email, line_number: Optional[int] = None
    ):
        if line_number is None:
            return

        self.line_numbers_for_email[casefolded_email].add(line_number)

    def _record_line_number_for_pair(
        self, casefolded_pair, line_number: Optional[int] = None
    ):
        if line_number is None:
            return

        self.line_numbers_for_pair[casefolded_pair].add(line_number)

    def _show_line_numbers(
        self,
        casefolded_emails: Optional[Iterable[str]] = None,
        casefolded_pairs: Optional[Iterable[tuple[str, str]]] = None,
    ):
        numbers = set()
        if casefolded_emails:
            for e in casefolded_emails:
                nums = self.line_numbers_for_email.get(e)
                if nums:
                    numbers.update(nums)
        if casefolded_pairs:
            for p in casefolded_pairs:
                nums = self.line_numbers_for_pair.get(p)
                if nums:
                    numbers.update(nums)
        if numbers:
            print(
                "    See locations:",
                ", ".join(f".mailmap:{num}" for num in sorted(numbers)),
            )

    def add(self, line: MailmapLine, line_number=None):
        """
        Add a parsed line to the check.

        line_number is optional but recommended for clearer error messages.
        """
        for op, match, replace in line.generate_operations():
            if op == OperationType.MATCH_EMAIL_SET_EMAIL:
                m_email = cast(str, match)
                self.canonicalize_emails[m_email].add(cast(str, replace))
                self._record_line_number_for_email(m_email, line_number)
            elif op == OperationType.MATCH_EMAIL_SET_NAME:
                m_email = cast(str, match)
                self.names[m_email].add(cast(str, replace))
                self._record_line_number_for_email(m_email, line_number)
            else:
                assert op == OperationType.MATCH_NAME_AND_EMAIL_SET_BOTH
                m: tuple[str, str] = cast(tuple[str, str], match)
                r: tuple[str, str] = cast(tuple[str, str], replace)
                self.full_matches[m].add(r)
                self._record_line_number_for_pair(m, line_number)

    def find_canonical_email_for_email(self, email: str):
        """
        Get the canonical email for a given email.

        Returns the input unchanged if not found or if more than one is found.
        """
        canonicals = self._get_canonical_emails(email)
        if not canonicals:
            return email

        if len(canonicals) > 1:
            return email

        return list(canonicals)[0]

    def find_canonical_name_for_email(self, email: str):
        """
        Get the canonical name for a given email.

        Returns None if not found or if more than one is found.
        """
        canonicals = self._get_canonical_names(email)
        if not canonicals:
            return None

        if len(canonicals) > 1:
            return None

        return list(canonicals)[0]

    def check_for_warnings(self) -> int:
        """Check for issues in the mailmap rules, returning the number of issues."""
        warnings = 0

        # Look at all mappings of casefolded email to sets of canonical emails
        for email, canonicals in self.canonicalize_emails.items():
            if len(canonicals) > 1:
                warnings += 1
                print(
                    f"Conflict: More than one canonical email for casefolded input email {email}:"
                )
                for c in canonicals:
                    print(f"  - {c}")
                self._show_line_numbers(casefolded_emails=[email])
            else:
                single_canonical: str = list(canonicals)[0]

                # Now, check to see if the canonical email in turn should map to something different.
                again = self.find_canonical_email_for_email(single_canonical)
                if again and again != single_canonical:
                    warnings += 1
                    print(f"Chain detected: {email} -> {single_canonical} -> {again}")
                    self._show_line_numbers(
                        casefolded_emails=[email, single_canonical.casefold()]
                    )

        # Look at all mappings of casefolded email to sets of canonical names
        for email, names in self.names.items():
            if len(names) > 1:
                warnings += 1
                print(
                    f"Conflict: More than one canonical name for casefolded input email {email}:"
                )
                for n in names:
                    print(f"  - {n}")
                self._show_line_numbers(casefolded_emails=[email])
            else:
                name: str = list(names)[0]
                replace_both = self.full_matches.get((name.casefold(), email))
                if replace_both:
                    warnings += 1
                    # new_name, new_email = replace_both
                    print(
                        f"Chain detected: {email} maps to name {name}, "
                        f"but that pair maps to",
                        [
                            format_person(new_name, new_email)
                            for new_name, new_email in replace_both
                        ],
                    )
                    self._show_line_numbers(
                        casefolded_emails=[email],
                        casefolded_pairs=[(name.casefold(), email)],
                    )

        # Look at all mappings of casefolded (name, email) to sets of canonical (name, email)
        for match, replacements in self.full_matches.items():
            _, match_email = match

            # Does the match email have canonical name or email on its own?
            sep_c_emails = self._get_canonical_emails(match_email)
            if sep_c_emails:
                warnings += 1
                print(
                    f"Conflict: A line exists to map only the pair {format_person(*match)}, "
                    f"but that email has its own canonicalization(s): {sep_c_emails}"
                )
                self._show_line_numbers(
                    casefolded_emails=[match_email],
                    casefolded_pairs=[match],
                )

            sep_c_names = self._get_canonical_names(match_email)
            if sep_c_names:
                warnings += 1
                print(
                    f"Conflict: A line exists to map only the pair {format_person(*match)}, "
                    f"but that email has its own canonical name(s): {sep_c_names}"
                )
                self._show_line_numbers(
                    casefolded_emails=[match_email],
                    casefolded_pairs=[match],
                )

            # Do we have more than one replacement for this (name, email)?
            if len(replacements) > 1:
                warnings += 1
                print(
                    f"Conflict: More than one canonical (name, email) pair "
                    f"for casefolded input pair {format_person(*match)}:"
                )
                for n in replacements:
                    print(f"   - {format_person(*n)}")
                self._show_line_numbers(
                    casefolded_pairs=[match],
                )
            else:
                # OK, only one replacement for this (name, email). Let's make sure it's terminal
                replacement: tuple[str, str] = list(replacements)[0]

                # Check for successive full matches
                transitive_matches = self._get_full_matches(*replacement)
                if transitive_matches:
                    warnings += 1
                    print(
                        f"Chain detected: {format_person(*match)} maps to {format_person(*replacement)}, "
                        f"but that pair maps to {[format_person(n, e) for n, e in transitive_matches]}>"
                    )
                    self._show_line_numbers(
                        casefolded_pairs=[
                            match,
                            (replacement[0].casefold(), replacement[1].casefold()),
                        ],
                    )

                _, rep_email = replacement

                # Check for successive canonical names and emails for the replacement email
                transitive_emails = self._get_canonical_emails(rep_email)
                if transitive_emails:
                    warnings += 1
                    print(
                        f"Chain detected: {format_person(*match)} maps to {format_person(*replacement)}, "
                        f"but that email maps to {transitive_emails}>"
                    )
                    self._show_line_numbers(
                        casefolded_emails=[rep_email.casefold()],
                        casefolded_pairs=[
                            match,
                        ],
                    )

                transitive_names = self._get_canonical_names(rep_email)
                if transitive_names:
                    warnings += 1
                    print(
                        f"Chain detected: {format_person(*match)} maps to {format_person(*replacement)}, "
                        f"but that email has its own canonical name(s): {transitive_names}"
                    )
                    self._show_line_numbers(
                        casefolded_emails=[rep_email.casefold()],
                        casefolded_pairs=[
                            match,
                        ],
                    )
        return warnings

    def _get_canonical_names(self, email):
        return self.names.get(email.casefold())

    def _get_canonical_emails(self, email):
        return self.canonicalize_emails.get(email.casefold())

    def _get_full_matches(self, name, email):
        return self.full_matches.get((name.casefold(), email.casefold()))


def script():
    """Run checker from command line."""
    parser = ArgumentParser()
    parser.add_argument("filename", help="Mailmap file name", nargs=1)
    args = parser.parse_args()
    main(args.filename[0])


if __name__ == "__main__":
    script()
