#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Various basic types related to mailmap files and their usage."""

from dataclasses import dataclass
from enum import Enum
import re
from typing import Optional, Union, cast

from .util import unwrap_email

_name_pattern = r"(?P<name>[^<#]+)"
_email_pattern = r"(?P<email><[^#>]+>)"
_oldname_pattern = _name_pattern.replace("?P<", "?P<old")
_oldemail_pattern = _email_pattern.replace("?P<", "?P<old")
_comment_pattern = r"(?P<comment>\s*#.*)?"

_LINE_RE = re.compile(
    r"(?P<data>({n}\s+)?{e}(?P<old>({on}? {oe}))?)?{c}".format(
        n=_name_pattern,
        e=_email_pattern,
        on=_oldname_pattern,
        oe=_oldemail_pattern,
        c=_comment_pattern,
    )
)


@dataclass(frozen=True)
class Identity:
    """A name and email pair."""

    name: str
    email: str

    def to_normalized_string(self):
        """Get this formatted nicely."""
        return f"{self.name.strip()} <{unwrap_email(self.email)}>"

    def to_casefolded_string_for_comparison(self):
        """Get this formatted nicely and casefolded."""
        return f"{self.name.strip().casefold()} <{unwrap_email(self.email.casefold())}>"


class OperationType(Enum):
    """Defines the meaning of the operation match and replace fields."""

    MATCH_EMAIL_SET_NAME = 1
    """match is a casefolded email, replace is a canonical name."""

    MATCH_EMAIL_SET_EMAIL = 2
    """match is a casefolded email, replace is a canonical."""

    MATCH_NAME_AND_EMAIL_SET_BOTH = 3
    """match is a casefolded name/email pair, replace is a canonical pair."""


@dataclass(frozen=True)
class Operation:
    """An operation produced by a mailmap line."""

    op_type: OperationType
    """The operation type"""

    match: Union[str, tuple[str, str]]
    """The data to match against the identity being processed."""

    replace: Union[str, tuple[str, str]]
    """The data to replace/set if the identity processed matches match."""

    def as_match_email_set_name(self) -> Optional[tuple[str, str]]:
        """Get the match, replace pair if a MATCH_EMAIL_SET_NAME."""
        if self.op_type != OperationType.MATCH_EMAIL_SET_NAME:
            return None
        return cast(str, self.match), cast(str, self.replace)

    def as_match_email_set_email(self) -> Optional[tuple[str, str]]:
        """Get the match, replace pair if a MATCH_EMAIL_SET_EMAIL."""
        if self.op_type != OperationType.MATCH_EMAIL_SET_EMAIL:
            return None
        return cast(str, self.match), cast(str, self.replace)

    def as_match_both(self) -> Optional[tuple[str, str]]:
        """Get the match name/email if a MATCH_EMAIL_SET_EMAIL."""
        if self.op_type != OperationType.MATCH_NAME_AND_EMAIL_SET_BOTH:
            return None
        return cast(str, self.match[0]), cast(str, self.match[1])

    def as_replace_both(self) -> Optional[tuple[str, str]]:
        """Get the replace name/email if a MATCH_EMAIL_SET_EMAIL."""
        if self.op_type != OperationType.MATCH_NAME_AND_EMAIL_SET_BOTH:
            return None
        return cast(str, self.match[0]), cast(str, self.match[1])


class MailmapLine:
    """
    A parsed line from a mailmap file.

    A line may be empty or have just a comment.

    The same rules apply for authors and committers, which I call "agents"
    here to unify them.
    For lines that have actual data, the git documentation defines four cases,
    which I have numbered as follows:

    * case 1: name <email>

      * Defines a canonical name for an email address, and canonical capitalization of that email address.

    * case 2: <email> <oldemail>

      * Defines an email to replace `oldemail` with.

      * Not clear from the docs whether the replacement result is subject to any further transformations

    * case 3: name <email> <oldemail>

      * Defines a canonical name and a canonical email address for all agents with email `oldemail`

      * That is, rreplace both name and email if oldemail matches

    * case 4: name <email> oldname <oldemail>

      * Defines a canonical (name, email) to use for only those agents matching **both** the `oldname` and `oldemail`.

    See https://git-scm.com/docs/gitmailmap

    The very-C source code of Git itself that does this is here: https://github.com/git/git/blob/master/mailmap.c though
    I think we will stick with regexes for parsing in this library.
    """

    def __init__(self, line: str):
        """Construct object."""
        self.line: str = line
        self.match = _LINE_RE.match(line)
        if not self.match:
            raise RuntimeError(f"Could not parse line: '{line}'")
        self.groupsdict = self.match.groupdict()
        if self._old_name and not self._name:
            # illegal form: having an old name implies having a canonical name
            raise RuntimeError("Got an old name without a canonical name: syntax error")

    @property
    def _email(self):
        return self.groupsdict["email"]

    @property
    def _name(self):
        return self.groupsdict["name"]

    @property
    def _old_email(self):
        return self.groupsdict["oldemail"]

    @property
    def _old_name(self):
        return self.groupsdict["oldname"]

    def _is_old_id_absent_or_redundant(self):
        oldname = self._old_name
        oldemail = self._old_email
        if not oldname and not oldemail:
            # No old anything: so yes, it's absent
            return True

        if oldname is not None:
            # We must compare new and old name and email
            return (
                self._name == oldname and self._email.casefold() == oldemail.casefold()
            )

        # No old name, just old email.
        return self._email.casefold() == oldemail.casefold()

    def has_old_identity(self, even_if_redundant=False):
        """
        Return true if there is some 'old' identity detail in this line.

        If the old identity data matches the new after casefolding,
        the return value depends on the even_if_redundant parameter.
        """
        oldname = self._old_name
        oldemail = self._old_email
        if not oldname and not oldemail:
            # No old anything
            return False
        # So we have an old something, but it might be redundant
        return even_if_redundant or not self._is_old_id_absent_or_redundant()

    def get_email(self, casefolded=False):
        """
        Return the (new) email address, optionally casefolded.

        This may be None if the line has no data.
        """
        email = self._email
        if email and casefolded:
            return email.casefold()
        return email

    def get_old_email(self, casefold=True, even_if_redundant=False):
        """
        Get the old email to match against.

        This may be None if the line has no data or in case 1.
        """
        if not self.has_old_identity(even_if_redundant=even_if_redundant):
            return None
        email = self.groupsdict["oldemail"].strip()
        if email and casefold:
            return email.casefold()
        return email

    def generate_operations(self):
        """Yield basic "operations" to apply the contents of this mailmap line."""
        name = self._name
        email = self._email
        oldname = self._old_name
        oldemail = self._old_email
        # case 1: name <email>
        # case 2: <email> <oldemail>
        # case 3: name <email> <oldemail>
        # case 4: name <email> oldname <oldemail>

        if not self.has_old_identity():
            # Case 1
            # That function takes care of old identities that are degenerate
            # due to matching the canonical one

            # No "old ID"

            if email is None:
                # just a comment
                return

            # Just a canonical name (and email capitalization) for an email:
            # case 1
            yield (OperationType.MATCH_EMAIL_SET_EMAIL, email.casefold(), email)
            yield (OperationType.MATCH_EMAIL_SET_NAME, email.casefold(), name)
            return

        if oldname:
            # We have an old name: we can only match both.
            # case 4
            yield (
                OperationType.MATCH_NAME_AND_EMAIL_SET_BOTH,
                (oldname.casefold(), oldemail.casefold()),
                (name, email),
            )
            # Does this also establish the same as case 1?
            yield (OperationType.MATCH_EMAIL_SET_EMAIL, email.casefold(), email)
            yield (OperationType.MATCH_EMAIL_SET_NAME, email.casefold(), name)
            return

        # case 2 or 3
        yield (OperationType.MATCH_EMAIL_SET_EMAIL, oldemail.casefold(), email)
        yield (OperationType.MATCH_EMAIL_SET_EMAIL, email.casefold(), email)

        if name:
            # think this doesn't actually happen in git
            # yield (Operation.MATCH_EMAIL_SET_NAME, email.casefold(), name)
            yield (OperationType.MATCH_EMAIL_SET_NAME, oldemail.casefold(), name)


class MailmapState:
    """Accumulate state from parsed mailmap lines."""

    def __init__(self):
        """Construct object."""
        # keys are all casefolded
        self.operations = set()
        self.canonical_emails_by_email: dict[str, str] = dict()
        self.canonical_names_by_email: dict[str, str] = dict()
        self.canonical_both_by_both: dict[tuple[str, str], tuple[str, str]] = dict()

    def add(self, line: MailmapLine, line_number=None):
        """Add a parsed line to the accumulated state."""
        had_effect = False
        for op, match, replace in line.generate_operations():
            self.got_operation = True
            if (op, match, replace) in self.operations:
                # we already have recorded this operation
                continue
            had_effect = True
            self.operations.add((op, match, replace))
            if op == OperationType.MATCH_EMAIL_SET_EMAIL:
                m_email = cast(str, match)
                self.canonical_emails_by_email[m_email] = cast(str, replace)

            elif op == OperationType.MATCH_EMAIL_SET_NAME:
                m_email = cast(str, match)
                self.canonical_names_by_email[m_email] = cast(str, replace)

            else:
                assert op == OperationType.MATCH_NAME_AND_EMAIL_SET_BOTH
                m: tuple[str, str] = cast(tuple[str, str], match)
                r: tuple[str, str] = cast(tuple[str, str], replace)
                self.canonical_both_by_both[m] = r
        return had_effect
