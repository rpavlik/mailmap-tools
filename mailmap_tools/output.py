#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Functionality for generating mailmap files."""
from itertools import chain
from typing import Optional

from .types import MailmapLine, MailmapState
from .util import format_person


class _MailmapBuilder:
    def __init__(self, header_lines: Optional[list[str]] = None):
        if header_lines is None:
            header_lines = []
        self.header_lines: list[str] = header_lines
        self.lines: list[str] = []
        self.state = MailmapState()

    def add_line(self, line: str):
        self.lines.append(line)

    def add_line_if_has_effect(self, line: str):
        new_parsed = MailmapLine(line)
        had_effect = self.state.add(new_parsed)
        if had_effect:
            self.lines.append(line)
            return True
        return False

    def get_contents_sorted(self):
        return "".join(
            f"{line}\n" for line in chain(self.header_lines, sorted(self.lines))
        )


class MailmapStateForMinimizing:
    """Like MailmapState but tracks original lines and comment headers."""

    def __init__(self) -> None:
        """Construct object."""
        self.header_lines: list[str] = []

        self.got_operation = False
        self.state = MailmapState()

        # for the original lines, when we want them
        self.lines: dict[str, str] = dict()

    def add(self, line: MailmapLine, line_number: Optional[int] = None):
        """Add a parsed line to the accumulated state."""
        if not self.got_operation and not line.get_email():
            # Treat this as a leading comment header.
            self.header_lines.append(line.line.rstrip())
            return False

        # Store the original case of the line.
        self.lines[line.line.casefold()] = line.line.rstrip()

        return self.state.add(line, line_number)

    def _find_original_case(self, line: str) -> str:
        orig_line = self.lines.get(line.casefold())
        if orig_line is not None:
            return orig_line
        return line

    def to_mailmap_lines(self):
        """
        Return the minimized contents.

        This re-constructs the mailmap file:
        it generates mailmap lines from the individual "operations" the
        original lines parsed into, in a specific minimal order, using
        the original formatting (case) of those lines if found, and only
        retains lines that add unique operations to a new scratch copy of
        the state.
        Thus, as long as this module's interpretation of mailmap semantics
        is accurate, this will produce a "minimal" version of the input file.
        Header lines (lines before the first line with actual data) are
        preserved without modification, and data lines are returned sorted.
        """
        builder = _MailmapBuilder(self.header_lines)

        # All the "case 4" mappings
        for match, replace in self.state.canonical_both_by_both.items():
            had_effect = builder.add_line_if_has_effect(
                self._find_original_case(
                    f"{format_person(*replace)} {format_person(*match)}"
                )
            )
            assert had_effect

        # All the lines with two emails
        for match_email, replace_email in self.state.canonical_emails_by_email.items():
            name = self.state.canonical_names_by_email.get(match_email)
            if match_email == replace_email.casefold():
                # only one email on this line:
                # skip until after we do all the lines with two emails,
                # they might make it unnecessary.
                continue
            if name is None:
                builder.add_line_if_has_effect(
                    self._find_original_case(f"{replace_email} {match_email}")
                )
            else:
                builder.add_line_if_has_effect(
                    self._find_original_case(
                        f"{format_person(name, replace_email)} {match_email}"
                    )
                )
        for match, replace in self.state.canonical_both_by_both.items():
            builder.add_line_if_has_effect(
                self._find_original_case(f"{format_person(*replace)}")
            )

        # All the lines with one email
        for email in self.state.canonical_emails_by_email.values():
            name = self.state.canonical_names_by_email.get(email.casefold())
            if name:
                builder.add_line_if_has_effect(format_person(name, email))

        # If we don't have any emails for which we only have a canonical name and
        # not email, we're good.
        assert not (
            set(self.state.canonical_names_by_email.keys())
            - set(self.state.canonical_emails_by_email.keys())
        )

        return builder.get_contents_sorted()
