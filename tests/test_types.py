#!/usr/bin/env python3
# Copyright 2024, Collabora, Ltd.
#
# SPDX-License-Identifier: GPL-3.0-only
#
# Original author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Tests for the mailmap tools."""

from mailmap_tools.types import MailmapLine

_case0_onlycomment = MailmapLine("    # comment")
_case1_lower = MailmapLine("Jill Bloggs <bloggs@example.com>")

_case1_upper = MailmapLine("Jill Bloggs <BLOGGS@example.com>")

_case1_comment = MailmapLine("Jill Bloggs <BLOGGS@example.com>       # comment")

_case2_upper = MailmapLine("<BLOGGS@example.com> <JBLOGGS@example.com>")

_case3_upper = MailmapLine("Jill Bloggs <BLOGGS@example.com> <JBLOGGS@example.com>")

# This one is tricky because the old email normalizes to the first
_case3_degenerate = MailmapLine("Jill Bloggs <bloggs@example.com> <BLOGGS@example.com>")

_case4_upper_comment = MailmapLine(
    "Jill Bloggs <BLOGGS@example.com> " "unk <JBLOGGS@example.com> # comment"
)


def test_get_old_email():
    """Check .get_old_email() method."""
    assert _case0_onlycomment.get_old_email() is None
    assert _case0_onlycomment.get_old_email(even_if_redundant=True) is None
    assert _case1_lower.get_old_email() is None
    assert _case1_lower.get_old_email(even_if_redundant=True) is None
    assert _case1_upper.get_old_email() is None
    assert _case1_upper.get_old_email(even_if_redundant=True) is None
    assert _case1_comment.get_old_email() is None
    assert _case1_comment.get_old_email(even_if_redundant=True) is None

    assert _case2_upper.get_old_email() == "<jbloggs@example.com>"
    assert _case2_upper.get_old_email(casefold=False) == "<JBLOGGS@example.com>"

    assert _case3_upper.get_old_email() == "<jbloggs@example.com>"
    assert _case3_upper.get_old_email(casefold=False) == "<JBLOGGS@example.com>"

    assert _case3_degenerate.get_old_email() is None
    assert (
        _case3_degenerate.get_old_email(even_if_redundant=True)
        == "<bloggs@example.com>"
    )

    assert _case4_upper_comment.get_old_email() == "<jbloggs@example.com>"
    assert _case4_upper_comment.get_old_email(casefold=False) == "<JBLOGGS@example.com>"


# def test_normalized():
#     """Check .normalized() method."""
#     assert _case0_onlycomment.normalized() == "    # comment"
#     assert _case1_lower.normalized() == "Jill Bloggs <bloggs@example.com>"
#     assert _case1_upper.normalized() == "Jill Bloggs <bloggs@example.com>"
#     assert (
#         _case1_comment.normalized()
#         == "Jill Bloggs <bloggs@example.com>       # comment"
#     )

#     assert _case2_upper.normalized() == "<bloggs@example.com> <JBLOGGS@example.com>"

#     assert (
#         _case3_upper.normalized()
#         == "Jill Bloggs <BLOGGS@example.com> <JBLOGGS@example.com>"
#     )

#     assert _case3_degenerate.normalized() is None
#     assert (
#         _case3_degenerate.normalized(even_if_redundant=True) == "<BLOGGS@example.com>"
#     )

#     assert _case4_upper_comment.normalized() == "<JBLOGGS@example.com>"
