# Mailmap Tools

<!--
Copyright 2024, Collabora, Ltd.

SPDX-License-Identifier: CC-BY-4.0
-->

Maintained at <https://gitlab.freedesktop.org/rpavlik/mailmap-tools>

Somehow, there had not yet been a module in Python that I could find to parse
and manipulate `.mailmap` files. So, I fixed that problem. This is mainly for my
own usage, but I share it in hopes it may help others as well.

## Commands

When installed (e.g. with `pipx`), two commands are made available:

- `mailmap_check` - Parses a mailmap file, looking for conflicting entries that
  need user attention to become consistent.
- `mailmap_minimize` - Perform some basic simplification on a mailmap file to
  remove redundant entries. **The file must pass `mailmap_check` successfully
  first or this will not work as expected.**

## Thanks

This tool was initially developed and maintained by Rylie Pavlik in the course
of her work at the open-source software consultancy
[Collabora](https://collabora.com). Thanks to Collabora and their "Open First"
philosophy for supporting the development of this software.

## License

The code itself is under "GPL-3.0-only" while this document is CC-BY-4.0. Other
files may be under CC0-1.0 if, in my opinion, they contained little to no
copyrightable content.

Every file has an SPDX license tag and copyright or author information in it
which is considered the authoritative licensing data.

This project is [REUSE-compliant](https://reuse.software) (version 3.0 of the
REUSE specification). You can use that project's tools to work with the
copyright and license notices of files in this project.
